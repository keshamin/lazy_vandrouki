FROM python:3.7-alpine
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh
RUN apk add tzdata && \
    cp /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    echo "Etc/UTC" > /etc/timezone && \
    date && \
    apk del tzdata
ENV dir /vandrouki_bot
RUN mkdir ${dir}
COPY . ${dir}
WORKDIR ${dir}
RUN pip install -r requirements.txt
CMD ["python", "bot.py"]
