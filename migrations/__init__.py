import importlib.util
import os
import re


migrations_dir = os.path.dirname(os.path.abspath(__file__))
migration_module_pattern = r'[\d]{3,5}_[\w]+.py'
migration_module_re = re.compile(migration_module_pattern)


def auto_migrations():
    for filename in sorted(os.listdir(migrations_dir)):
        if migration_module_re.match(filename):
            path_to_migration = os.path.join(migrations_dir, filename)
            spec = importlib.util.spec_from_file_location('migration', path_to_migration)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            del module, spec
