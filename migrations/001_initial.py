from peewee import SqliteDatabase
import os
from playhouse.migrate import *

from config import DB_PATH
from models import User, KeywordGroup, Keyword, NewUserRequest, MigrationLog
from log import logger


db = SqliteDatabase(DB_PATH, pragmas={'foreign_keys': 1})
db.connect()
migrator = SqliteMigrator(db)

MIGRATION_ID = int(os.path.basename(__file__).split('_')[0])
migration, migration_not_applied = MigrationLog.get_or_create(migration_id=MIGRATION_ID)


if migration_not_applied:

    logger.info(f'Migration #{MIGRATION_ID} is applying...')

    # --- Migration code goes here: ---
    db.create_tables([User, KeywordGroup, Keyword, NewUserRequest])

    # --- Migration code ends here. ---
else:
    logger.info(f'Migration #{MIGRATION_ID} is already applied.')

db.close()
